# Materiale del corso
In questo repository caricheremo volta per volta le slide delle lezioni ed eventuale altro materiale utile

## Link diretti
- [Lezione 1 - Introduzione](https://gitlab.com/corso_webapp/slide/raw/master/lezione1_introduzione.pdf?inline=false)
- [Lezione 2 - Configurazione](https://gitlab.com/corso_webapp/slide/raw/master/lezione2_configurazione.pdf?inline=false)
- [Lezione 3 - Wreck the (hello) world](https://gitlab.com/corso_webapp/slide/raw/master/lezione3_wthw.pdf?inline=false)
- [Lezione 4 - ReApp](https://gitlab.com/corso_webapp/slide/raw/master/lezione4_reapp.pdf?inline=false)

### Contatti
Scriveteci una mail a:
- paolo.bonato.3@studenti.unipd.it
- tommaso.padovan@studenti.unipd.it
- matteo.sovilla@studenti.unipd.it

Per ulteriori comunicazioni utilizzeremo il canale Telegram che trovate al seguente link:
> https://t.me/joinchat/AAAAAEidguJiw_8hGQD5iA